﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Drawing.Drawing2D;
using System.Reflection;

namespace ZAMG_Wetterdaten
{
    public partial class Form1 : Form
    {        
        OleDbConnection connection = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0;Data Source=zamg.mdb");
        const int X_VAL = 50;
        const int Y_VAL = 70;
        string[] months = new string[12] { "Jan", "Feb", "Mar", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez" };
        string[] temps = new string[6] {"10", "20", "30", "- 10", "- 20", "- 30" };        
        int x = 40;
        int y = 350;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            FillComboBox();                    
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Matrix m = new Matrix();
            Graphics g = e.Graphics;
            Pen p = new Pen(Color.Black, 2);
            Font f = new Font("Arial", 10);
            SolidBrush sb = new SolidBrush(Color.Black);
            StringFormat sf = new StringFormat();            
            
            m.Translate(x, y);
            g.Transform = m;         

            g.DrawLine(p, -5, 0, X_VAL * 13, 0); //x-Achse
            g.DrawLine(p, 0, -Y_VAL * 3 - 50, 0, Y_VAL * 3 + 50); //y-Achse
            
            g.DrawString("0", f, sb, -15, -9, sf); //Ursprung
            g.DrawString("°C", f, sb, 15, - Y_VAL * 3.8F, sf); //y-Achsen Beschriftung
            g.DrawString("Monat", f, sb, X_VAL * 13.3F, -9, sf); //x-Achsen Beschriftung
            g.DrawString("Legende:", f, sb, X_VAL * 13, - Y_VAL * 4, sf);//Legende

            int vy = 0;
            for (int i = 0; i < 3; i++) //Striche und Beschriftung auf y-Achse
            {
                vy += Y_VAL;
                g.DrawLine(p, -10, -vy, 10, -vy);
                g.DrawString(temps[i], f, sb, -30, -vy - 9, sf);
                g.DrawLine(p, -10, vy, 10, vy);
                g.DrawString(temps[i+3], f, sb, -35, vy - 9, sf);
            }

            int vx = 0;
            for (int i = 0; i < 12; i++) //Striche und Beschriftung auf x-Achse
            {
                vx += X_VAL;
                g.DrawLine(p, vx, -10, vx, 10);
                g.DrawString(months[i], f, sb, vx-15, 15, sf);
            }        
        }

        private void yearBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            Refresh();                          
            List<string> cities = GetCities();
            //List<Coordinates> cord = new List<Coordinates>();
            string year = yearBox.Text;
            connection.Open();
            OleDbCommand cmd;
            OleDbDataReader reader;
            Graphics g = this.CreateGraphics();
            Pen p;
            Font f = new Font("Arial", 10);
            SolidBrush sb = new SolidBrush(Color.Black);
            StringFormat sf = new StringFormat();
            Matrix m = new Matrix();
            m.Translate(x, y);
            g.Transform = m;

            float x1, y1, x2, y2;
            int l = 25;
            int ltemp = 0;
            
            float alt = 0;
            float neu = 0;

            int vx = 0;
            foreach (var item in cities)
            {
                ltemp += l;
                p = new Pen(RandomColor(), 3);
                g.DrawString(item, f, sb, X_VAL * 13 + 50, -Y_VAL * 4 + ltemp, sf);
                g.DrawLine(p, X_VAL * 13, -Y_VAL * 4 + ltemp + 8, X_VAL * 13 + 40, -Y_VAL * 4 + ltemp + 8);
                cmd = new OleDbCommand("SELECT * FROM wetterdaten WHERE Stadt = \"" + item + "\" AND Jahr = " + year + "", connection);
                reader = cmd.ExecuteReader();           
                while (reader.Read())
                {
                    alt = 0;                    
                    for (int i = 2; i <= 13; i++)
                    {
                        vx += X_VAL;                        
                        neu = reader.GetFloat(i);
                        x1 = vx;
                        y1 = -Y_VAL / 10 * alt;
                        x2 = vx + X_VAL;
                        y2 = -Y_VAL / 10 * neu;
                        g.DrawLine(p, x1 - X_VAL, y1, x2 - X_VAL, y2);
                        //cord.Add(new Coordinates(x1, y1, x2, y2));
                        alt = neu;
                    }
                    vx = 0;                    
                }

                //string t = "";
                //foreach (var c in cord)
                //{
                //    t += c.x1 + " " + c.y1 + " " + c.x2 + " " + c.y2 + "\r\n";
                //}               
                //MessageBox.Show(t);
            }
            connection.Close();
        }

        public void FillComboBox() //ComboBox mit Jahren befüllen
        {
            connection.Open();
            OleDbCommand cmd = new OleDbCommand("SELECT DISTINCT Jahr FROM wetterdaten;", connection);
            OleDbDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                yearBox.Items.Add(reader.GetValue(0));
            }
            connection.Close();
        }

        public List<string> GetCities() //Liste mit Städten erzeugen um später jeweils die Wetterdaten zu erhalten
        {
            string year = yearBox.Text;
            List<string> cities = new List<string>();
            connection.Open();
            OleDbCommand cmd = new OleDbCommand("SELECT Stadt FROM wetterdaten WHERE Jahr = " + year + "", connection);
            OleDbDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                cities.Add(reader.GetString(0));
            }
            connection.Close();
            return cities;           
        }

        public static List<Color> ColorStructToList()
        {
            return typeof(Color).GetProperties(BindingFlags.Static | BindingFlags.DeclaredOnly | BindingFlags.Public)
                                .Select(c => (Color)c.GetValue(null, null))
                                .ToList();
        }

        private Color RandomColor()
        {
            List<Color> myList = ColorStructToList();
            Random random = new Random();
            Color color = myList[random.Next(myList.Count - 1)];
            return color;
        }
    }
}
