﻿#define LOG
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Server
{
    class Program
    {
        static TcpListener listener;
        const int LIMIT = 5; //5 concurrent clients
        static List<int> lottoZahlen = new List<int>();
        static Random rand = new Random();

        static void Main(string[] args)
        {
            listener = new TcpListener(2055);
            listener.Start();
#if LOG
            Console.WriteLine("Server mounted, listening to port 2055");
#endif
            for (int i = 0; i < LIMIT; i++)
            {
                Thread t = new Thread(new ThreadStart(Service));
                t.Start();
            }
        }

        static List<int> gewinnZahlen;
        static List<int> quickTipp;
        public static void Service()
        {
            while (true)
            {
                Socket soc = listener.AcceptSocket();
                //soc.SetSocketOption(SocketOptionLevel.Socket,
                //        SocketOptionName.ReceiveTimeout,10000);
#if LOG
                Console.WriteLine("Connected: {0}", 
                                         soc.RemoteEndPoint);
#endif
                try
                {
                    Stream s = new NetworkStream(soc);
                    StreamReader sr = new StreamReader(s);
                    StreamWriter sw = new StreamWriter(s);
                    sw.AutoFlush = true; // enable automatic flushing
                    gewinnZahlen = GenerateNumbers();
                    string request = sr.ReadLine().ToUpper();
                    string quickString;
                    string gewinnString;
                    string matchString;
                    List<int> matchList;
                    int matches;
                    for (int i = 1; i <= 45; i++)
                    {
                        lottoZahlen.Add(i);
                    }
                    while (request == "Q")
                    {
                        quickTipp = GenerateNumbers();
                        //List<int> gewinnZahlen = GenerateNumbers();                        
                        quickString = "";
                        gewinnString = "";
                        matchString = "";

                        quickTipp.Sort();
                        gewinnZahlen.Sort();

                        foreach (var item in gewinnZahlen)
                        {
                            gewinnString += item + "-";
                        }
                        gewinnString = gewinnString.Substring(0, gewinnString.Length - 1);
                       // Console.WriteLine("Gewinn-Zahlen: " + gewinnString);

                        //for (int i = 1; i <= 45; i++)
                        //{
                        //    lottoZahlen.Add(i);
                        //}

                        //request = sr.ReadLine().ToUpper();

                        for (int i = 0; i < quickTipp.Count; i++)
                        {
                            quickString += quickTipp[i].ToString() + "-";
                        }
                        quickString = quickString.Substring(0, quickString.Length - 1);
                        //Console.WriteLine("Qicktipp:" + quickString);
                        sw.WriteLine("Dein Quicktipp:" + quickString);

                        matchList = CompareLists(quickTipp, gewinnZahlen);

                        if (matchList.Count > 0)
                        {
                            foreach (var item in matchList)
                            {
                                matchString += item + "-";
                            }
                            matchString = matchString.Substring(0, matchString.Length - 1);
                        }
                        else
                            matchString = "Verloren!";

                        matches = matchList.Count;
                        if (matches == 6)
                            matchString = "Jackpot!";
                        //Console.WriteLine(matches + " Übereinstimmungen: " + matchString);

                        sw.WriteLine(matches + " Übereinstimmungen: " + matchString);

                        request = sr.ReadLine().ToUpper();
                    }

                    s.Close();
                }
                catch (Exception e)
                {
#if LOG
                    Console.WriteLine(e.Message);
#endif
                }
#if LOG
                Console.WriteLine("Disconnected: {0}", 
                                        soc.RemoteEndPoint);
#endif
                soc.Close();
            }
        }

        static List<int> quick;
        static public List<int> GenerateNumbers()
        {
            quick = new List<int>();
            for (int i = 0; i < 6; i++)
            {
                int zahl = rand.Next(1, 46);

                if (!quick.Contains(zahl))
                {
                    quick.Add(zahl);
                }
                else
                {
                    i--;
                }
            }

            return quick;
        }

        static List<int> matchList;
        static public List<int> CompareLists(List<int> quickTipp, List<int> gewinnZahlen)
        {
            matchList = new List<int>();

            foreach (var g in gewinnZahlen)
            {
                foreach (var q in quickTipp)
                {
                    if (g == q)
                    {
                        matchList.Add(q);
                    }
                }
            }

            return matchList;
        }
    }
}
