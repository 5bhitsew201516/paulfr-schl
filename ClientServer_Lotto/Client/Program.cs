﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    class Program
    {
        static void Main(string[] args)
        {
            TcpClient client = new TcpClient("localhost", 2055);
            try
            {
                Stream s = client.GetStream();
                s.ReadTimeout = 100;
                StreamReader sr = new StreamReader(s);

                StreamWriter sw = new StreamWriter(s);
                sw.AutoFlush = true;
                /*
                string input = Console.ReadLine().ToUpper();
                while (input == "Q")
                {                   
                    sw.WriteLine(input);

                    string antwort = sr.ReadLine();
                    Console.WriteLine(antwort);

                    antwort = sr.ReadLine();
                    Console.WriteLine(antwort);

                    input = Console.ReadLine().ToUpper();
                }
                */

                string input = "Q";
                string antwort = "";
                int counter = 0;
                Stopwatch w = Stopwatch.StartNew();
                while (!antwort.StartsWith("6 "))
                {
                    counter++;
                    sw.WriteLine(input);

                    antwort = sr.ReadLine();
                    //Console.WriteLine(antwort);

                    antwort = sr.ReadLine();
                    //Console.WriteLine(antwort);
                    //Console.WriteLine("Versuche: " + counter);
                }
                w.Stop();
                Console.WriteLine(antwort);
                Console.WriteLine("Versuche: " + counter);
                Console.WriteLine("Dauer: " + w.Elapsed);
                Console.ReadKey();
                s.Close();
            }
            finally
            {
                // code in finally block is guranteed 
                // to execute irrespective of 
                // whether any exception occurs or does 
                // not occur in the try block
                client.Close();
            }
        }        
    }
}
