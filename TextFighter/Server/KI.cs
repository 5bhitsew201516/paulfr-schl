﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class KI
    {
        Random r = new Random();

        public string SelectWeapon(List<Weapon> list)
        {
            int rand = r.Next(list.Count);
            rand += 1;
            return rand.ToString();
        }

        public string SelectPotion(List<Potion> list)
        {
            int rand = r.Next(list.Count);
            rand += 1;
            return rand.ToString();
        }
    }
}
