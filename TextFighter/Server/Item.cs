﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class Item
    {
        public string id;
        public string name;
        public int costs;

        public Item(string id, string name, int costs)
        {
            this.id = id;
            this.name = name;
            this.costs = costs;
        }
    }
}
