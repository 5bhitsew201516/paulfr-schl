﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class Game
    {
        public bool checkIfWon(Player player1, Player player2)
        {
            if (player1.currentHP <= 0 || player2.currentHP <= 0)
                return true;
            else
                return false;
        }

        public string whoWon(Player player1, Player player2)
        {
            if (player1.currentHP <= 0)
                return "Spieler 2 hat gewonnen";
            else /*if (player2.currentHP <= 0)*/
                return "Spieler 1 hat gewonnen";

        }

        public int useWeapon(List<Weapon> weaponList, string wid)
        {
            Random r = new Random();
            var query = (from w in weaponList
                         where w.id.Equals(wid)
                         select new { w.minDmg, w.maxDmg }).First();
                        
            int dmg = r.Next(query.minDmg, query.maxDmg);
            return dmg;
        }

        public string usePotions(List<Potion> potionList, string pid, Player player)
        {
            var query = (from p in potionList
                         where p.id.Equals(pid)
                         select p.healAmount).First();

            player.currentHP += query;

            if (player.currentHP > player.maxHP)
                player.currentHP = player.maxHP;

            return player.name + " hat jetzt " + player.currentHP + " Trefferpunkte$";
        }

        public string showWeaponList(List<Weapon> list)
        {
            string weaponString = "Wähle eine Waffe:$";
            foreach (var item in list)
            {
                weaponString += item.id + ": " + item.name + " (" + item.costs + ") " + "$";
            }

            return weaponString;
        }

        public string showPotionList(List<Potion> list)
        {
            string potionString = "Wähle einen Trank:$";
            foreach (var item in list)
            {
                potionString += item.id + ": " + item.name + " (" + item.costs + ") " + "$";
            }

            return potionString;
        }

        public Game()
        {
            
        }
    }
}
