﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class Weapon : Item
    {
        public int minDmg { get; set; }
        public int maxDmg { get; set; }

        public Weapon(string id, string name, int costs, int minDmg, int maxDmg):base(id, name, costs)
        {
            this.id = id;
            this.name = name;
            this.minDmg = minDmg;
            this.maxDmg = maxDmg;
        }
    }
}
