﻿#define LOG
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Server
{
    class Server
    {
        static TcpListener listener;
        const int LIMIT = 5; //5 concurrent clients

        static List<Weapon> weapons = new List<Weapon>() {
                    new Weapon("0", "Keine Aktion", 0, 0, 0),
                    new Weapon("1", "DH-17", 0, 1, 10),
                    new Weapon("2", "DLT-99", 10, 25, 50),
                    new Weapon("3", "RT-97C", 20, 55, 75),
                    new Weapon("4", "A280C", 5, 20, 40),
                    new Weapon("5", "CA-87", 30, 20, 80),
                    new Weapon("6", "E-11", 15, 20, 55),
                    new Weapon("7", "T-21", 25, 15, 70),
                    new Weapon("8", "SE-14C", 3, 5, 10),
                    new Weapon("9", "EE-3", 35, 50, 75),
                    new Weapon("10", "T-21B", 40, 25, 85),
                    new Weapon("11", "DL-44", 2, 1, 20)
                };

        static List<Potion> potions = new List<Potion>(){
                    new Potion("0", "Keine Aktion", 0, 0),
                    new Potion("1", "Kleiner Heiltrank", 20,  25),
                    new Potion("2", "Mittlerer Heiltrank", 40, 50),
                    new Potion("3", "Großer Heiltrank", 60, 75),
                    new Potion("4", "Zaubertrank der höchsten Heilung", 80, 100)
                };

        public static void Main()
        {
            listener = new TcpListener(2055);
            listener.Start();            
#if LOG
            Console.WriteLine("Server mounted, listening to port 2055");
#endif
            for (int i = 0; i < LIMIT; i++)
            {
                Thread t = new Thread(new ThreadStart(Service));
                t.Start();
            }
        }
        public static void Service()
        {
            while (true)
            {
                Socket soc = listener.AcceptSocket();
#if LOG
                Console.WriteLine("Connected: {0}", 
                                         soc.RemoteEndPoint);

                Game g = new Game();
                KI ki = new KI();

                Player player1 = new Player("Spieler 1", 100, 100, 200);
                Player server = new Player("Server", 100, 100, 200);

                string weaponString = g.showWeaponList(weapons);
                string potionString = g.showPotionList(potions);
#endif
                try
                {
                    Stream s = new NetworkStream(soc);
                    StreamReader sr = new StreamReader(s);
                    StreamWriter sw = new StreamWriter(s);
                    sw.AutoFlush = true; // enable automatic flushing

                    string request = "";
                    string answer = "";
                    int dmg = 0;

                    do
                    {
                        Console.WriteLine("Spieler 1 ist an der Reihe");
                        sw.WriteLine(weaponString);
                        request = sr.ReadLine();
                        Console.WriteLine("Erhalten " + request);
                        dmg = g.useWeapon(weapons, request);
                        server.currentHP = server.currentHP - dmg;
                        sw.WriteLine("$Spieler 2 wurde mit " + dmg + " Schaden getroffen. Er hat noch " + server.currentHP + " Trefferpunkte.$");
                        Console.WriteLine("Spieler 2: " + dmg + " Schaden erhalten; noch: " + server.currentHP + " Trefferpunkte\r\n");
                        //Trank auswählen
                        sw.WriteLine(potionString);
                        request = sr.ReadLine();
                        Console.WriteLine("Erhalten " + request);
                        answer = g.usePotions(potions, request, player1);
                        Console.WriteLine(answer);
                        sw.WriteLine(answer);

                        Console.WriteLine("------------------------------");

                        Console.WriteLine("Server ist an der Reihe");
                        sw.WriteLine("$Server ist an der Reihe$");
                        request = ki.SelectWeapon(weapons);
                        dmg = g.useWeapon(weapons, request);
                        player1.currentHP = player1.currentHP - dmg;
                        sw.WriteLine("$Spieler 1 wurde mit " + dmg + " Schaden getroffen. Er hat noch " + player1.currentHP + " Trefferpunkte.$");
                        Console.WriteLine("Spieler 1: " + dmg + " Schaden erhalten; noch: " + player1.currentHP + " Trefferpunkte\r\n");
                        //Trank auswählen
                        request = ki.SelectPotion(potions);
                        answer = g.usePotions(potions, request, server);
                        Console.WriteLine(answer);
                        sw.WriteLine(answer);
                        //sw.WriteLine(weaponString);
                        //request = sr.ReadLine();
                        //Console.WriteLine("Erhalten " + request);
                        //dmg = g.useWeapon(weapons, request);
                        //player1.currentHP = player1.currentHP - dmg;
                        //sw.WriteLine("$Spieler 1 wurde mit " + dmg + " getroffen. Er hat noch " + player1.currentHP + " Trefferpunkte.$");
                        //Trank auswählen
                        //sw.WriteLine(potionString);
                        //request = sr.ReadLine();
                        //Console.WriteLine("Erhalten " + request);
                        //answer = g.usePotions(potions, request, player2);
                        //sw.WriteLine(answer);
                        
                        Console.WriteLine("------------------------------");
                    } while (!g.checkIfWon(player1, server));

                    if (g.checkIfWon(player1, server))
                    {
                        answer = g.whoWon(player1, server);
                        sw.WriteLine("GAME-OVER$" + answer);
                    }
                       

                    s.Close();
                }
                catch (Exception e)
                {
#if LOG
                    Console.WriteLine(e.Message);
#endif
                }
#if LOG
                Console.WriteLine("Disconnected: {0}", 
                                        soc.RemoteEndPoint);
#endif
                soc.Close();
            }
        }

    }
}