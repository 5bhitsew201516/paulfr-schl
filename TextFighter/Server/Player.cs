﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class Player
    {
        public string name { get; set; }
        public int maxHP { get; set; }
        public int currentHP { get; set; }
        public int money { get; set; }

        public Player(string name, int hp, int currentHP, int money)
        {
            this.name = name;
            this.maxHP = hp;
            this.currentHP = currentHP;
            this.money = money;
        }
    }
}
