﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class Potion : Item
    {
        public int healAmount { get; set; }

        public Potion(string id, string name, int costs, int amount) : base(id, name, costs)
        {
            this.healAmount = amount;
        }
    }
}
