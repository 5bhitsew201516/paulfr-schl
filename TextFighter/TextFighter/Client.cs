﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace TextFighter
{
    class Client
    {
        public static void Main(string[] args)
        {
            Timer myTimer = new Timer();
            myTimer.Elapsed += new ElapsedEventHandler(TimerEvent);
            myTimer.Interval = 5000;
            myTimer.Enabled = false;

            TcpClient client = new TcpClient("localhost", 2055);
            try
            {
                Stream s = client.GetStream();
                s.ReadTimeout = 100;
                StreamReader sr = new StreamReader(s);

                StreamWriter sw = new StreamWriter(s);
                sw.AutoFlush = true;

                string id = "";
                string msg = "";
                string format = ""; //formatireung für neue Zeile                

                /*sw.WriteLine(eingabe);
                sw.Flush();
                sw.Flush();
                message = sr.ReadLine();
                Console.WriteLine("\nEmpfangen: " + message);*/
                
                do
                {
                    Console.WriteLine("Spieler 1 ist an der Reihe.");
                    msg = sr.ReadLine();
                    format = msg.Replace("$", Environment.NewLine); //neue Zeile bei "$"
                    Console.WriteLine(format);
                    id = Console.ReadLine();
                    //Console.WriteLine("Eingabe: " + id);
                    sw.WriteLine(id);
                    msg = sr.ReadLine();
                    format = msg.Replace("$", Environment.NewLine);
                    Console.WriteLine(format);
                    //Trank Auswählen
                    msg = sr.ReadLine();
                    format = msg.Replace("$", Environment.NewLine);
                    Console.WriteLine(format);
                    id = Console.ReadLine();
                    Console.WriteLine("Eingabe: " + id);
                    sw.WriteLine(id);
                    msg = sr.ReadLine();
                    format = msg.Replace("$", Environment.NewLine);
                    Console.WriteLine(format);

                    Console.WriteLine("------------------------------");

                    msg = sr.ReadLine();
                    format = msg.Replace("$", Environment.NewLine);
                    Console.WriteLine(format);
                    msg = sr.ReadLine();
                    format = msg.Replace("$", Environment.NewLine);
                    Console.WriteLine(format);
                    //Trank
                    msg = sr.ReadLine();
                    format = msg.Replace("$", Environment.NewLine);
                    Console.WriteLine(format);
                    //Console.WriteLine("Spieler 2 ist an der Reihe.");
                    //msg = sr.ReadLine();
                    //format = msg.Replace("$", Environment.NewLine);
                    //Console.WriteLine(format);
                    //id = Console.ReadLine();
                    //Console.WriteLine("Eingabe: " + id);
                    //sw.WriteLine(id);
                    //msg = sr.ReadLine();
                    //format = msg.Replace("$", Environment.NewLine);
                    //Console.WriteLine(format);
                    ////Trank Auswählen
                    //msg = sr.ReadLine();
                    //format = msg.Replace("$", Environment.NewLine);
                    //Console.WriteLine(format);
                    //id = Console.ReadLine();
                    //Console.WriteLine("Eingabe: " + id);
                    //sw.WriteLine(id);
                    //msg = sr.ReadLine();
                    //format = msg.Replace("$", Environment.NewLine);
                    //Console.WriteLine(format);
                    Console.WriteLine("------------------------------");
                } while (!msg.StartsWith("GAME-OVER"));

                Console.ReadKey();
                s.Close();
            }
            finally
            {
                // code in finally block is guranteed 
                // to execute irrespective of 
                // whether any exception occurs or does 
                // not occur in the try block
                client.Close();
            }
        }

        public static void TimerEvent(object source, ElapsedEventArgs e)
        {
            Console.WriteLine("Hallo Timer Event");
        }
    }
}
