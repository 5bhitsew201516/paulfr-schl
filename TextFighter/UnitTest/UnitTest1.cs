﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Server;
using System.Collections.Generic;

namespace UnitTest
{
    [TestClass]
    public class UnitTest1
    {
        Game g = new Game();

        Player player1 = new Player("Spieler 1", 100, 55, 50);
        Player player2 = new Player("Spieler 2", 100, 0, 50);

        List<Weapon> weapons = new List<Weapon>() {
            new Weapon("1", "Stock", 1, 1, 1)            
        };
        List<Potion> potions = new List<Potion>() {
            new Potion("1", "Heiltrank", 20, 50)
        };       
            

        [TestMethod]
        public void ListTest()
        {
           

            Assert.AreEqual(g.showWeaponList(weapons), "Wähle eine Waffe:$1: Stock (1) $");
            Assert.AreEqual(g.showPotionList(potions), "Wähle einen Trank:$1: Heiltrank (20) $");

            Assert.AreEqual(g.useWeapon(weapons, "1"), 1);

            Assert.AreEqual(g.usePotions(potions, "1", player1), player1.name + " hat jetzt 100 Trefferpunkte$");
        }

        [TestMethod]
        public void PlayerTest()
        {           
            Assert.AreEqual(g.checkIfWon(player1, player2), true);
            Assert.AreEqual(g.whoWon(player1, player2), "Spieler 1 hat gewonnen");
            player2.currentHP = 1;
            player1.currentHP = 0;
            Assert.AreEqual(g.checkIfWon(player1, player2), true);
            Assert.AreEqual(g.whoWon(player1, player2), "Spieler 2 hat gewonnen");
        }

        [TestMethod]
        public void KITest()
        {
            KI ki = new KI();

            string id = ki.SelectWeapon(weapons);
            Assert.AreEqual(id, "1");

            string pid = ki.SelectPotion(potions);
            Assert.AreEqual(pid, "1");            
        }
    }
}
